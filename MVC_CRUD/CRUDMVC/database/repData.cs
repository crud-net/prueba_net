﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRUDMVC.database
{
    public class repData
    {
        DataClasses1DataContext db = new DataClasses1DataContext(System.Configuration.ConfigurationManager.ConnectionStrings["dbWork"].ToString());
        //Get all jobs in a list
        public List<job> getJobs()
        {
            return (from j in db.job select j).ToList();
        }
        //Get a job
        public job getJob(int id)
        {
            return (from j in db.job where j.id == id select j).FirstOrDefault();
        }
        //Save or update job
        public bool saveJob(job pJob)
        {
            job objJob = new job();
            if(pJob.id == 0)
            {
                pJob.CreatedAt = DateTime.Now;
                db.job.InsertOnSubmit(pJob);
                db.SubmitChanges();
                if (pJob.id != 0)
                    return true;
                else
                    return false;
            }
            else
            {
                objJob = (from j in db.job where j.id == pJob.id select j).FirstOrDefault();
                objJob.Job1 = pJob.Job1;
                objJob.JobTitle = pJob.JobTitle;
                objJob.Description = pJob.Description;
                objJob.ExpiresAt = pJob.ExpiresAt;
                db.SubmitChanges();
                return true;
            }
           
        }
        //Delete a job
        public bool delJob (int id)
        {
            job pJob = (from j in db.job where j.id == id select j).FirstOrDefault();
            try
            {
                db.job.DeleteOnSubmit(pJob);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}