﻿$('document').ready(function () {
    ///Create or update a Job
    $('body').on('click', '#saveNewJob', function (e) {        
        var id = $("#inpId").val();
        var job = $("#inpJob").val();
        var title = $("#inpJobTitle").val();
        var descripcion = $("#inpJobDescription").val();
        var expires = $("#inpJobExpires").val();
        if (job.length != 0 && title.length != 0 && descripcion.length != 0 && expires.length != 0) {
            var obj = { "id": id, "Job1": job, "JobTitle": title, "Description": descripcion, "ExpiresAt": expires };
            $.ajax({
                url: $("#urlNewJob").val(),
                type: 'POST',
                data: obj,
                success: function (obj) {
                    CorrectAction();
                    if (id == 0)
                        $('input[type="text"], input[type="date"], textarea').val('');
                },
                error: function (objResult) {
                    FailureAction();
                }               
            });
        }
        else {
            $("#myModal .modal-content").removeClass("CorrectRequest").addClass("FailedReques");;            
            $('#myModal').modal('toggle');
            $("#myModalLabelHead").text("Incomplete data");
            $("#myModalLabelBody").text("Make sure you fill in the fields correctly");
        }
    });

    ///Delete confirmation message
    $('body').on('click', '.deleteConfirm', function (e) {
        var code = $(this).data("code");
        $('#myModalQuestion').modal('toggle');
        $("#myModalQuestionLabelHead").text("Confirmation")
        $("#myModalQuestionLabelBody").text("Are yoy sure to delete this permanently?")
        $("#idDeleteJob").val(code);       
    });

    ////Delete a Job
    $('body').on('click', '#btn-deleteJob', function (e) {
        var id = $("#idDeleteJob").val();        
        if (id != 0) {
            var objJson = { "id": id };
            $.ajax({
                url: $("#urlDeleteJob").val(),
                type: 'POST',
                data: objJson,
                success: function (objJson) {
                    CorrectAction();
                    $("#" + id).parent().fadeOut();
                },
                error: function (objResult) {
                    FailureAction();
                }
            });
        }
    });
});

function CorrectAction() {
    $("#myModal .modal-content").removeClass("FailedReques").addClass("CorrectRequest");
    $('#myModal').modal('toggle');
    $("#myModalLabelHead").text("Action completed");
    $("#myModalLabelBody").text("The action was completed successfully");
}
function FailureAction() {
    $("#myModal .modal-content").removeClass("CorrectRequest").addClass("FailedReques");
    $('#myModal').modal('toggle');
    $("#myModalLabelHead").text("Action not completed");
    $("#myModalLabelBody").text("An error occurred, try later");
}