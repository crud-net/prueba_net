﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRUDMVC.database;

namespace CRUDMVC.Controllers
{
    public class JobController : Controller
    {
        repData dbFunctions = new repData();
        // GET: Job
        public ActionResult Index()
        {
            List<job> jobsList = dbFunctions.getJobs();
            return View(jobsList);
        }
        // GET: Job/Create
        public ActionResult newJob(int id = 0)
        {
            job pJob = new job();
            if(id != 0)
            {
                pJob = dbFunctions.getJob(id);
            }
            return View(pJob);
        }
        [HttpPost]
        public ActionResult configJob(job pJob)
        {
            string message = "";            
            bool r = dbFunctions.saveJob(pJob);
            if (r)
                message = "okay";
            else
                message = "fail";
            return Json(new { Message = message, JsonRequestBehavior.AllowGet });
        }
        // POST: Job/Delete/5
        [HttpPost]
        public ActionResult DeleteJob(int id)
        {
            string message = "";
            bool r = dbFunctions.delJob(id);
            if (r)
                message = "okay";
            else
                message = "fail";
            return Json(new { Message = message, JsonRequestBehavior.AllowGet });
        }

    }
}
